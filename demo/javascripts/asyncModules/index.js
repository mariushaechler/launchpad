const moduleElements = document.querySelectorAll('[data-module]');

for (let i = 0; i < moduleElements.length; i += 1) {
  const el = moduleElements[i];
  const name = el.getAttribute('data-module');
  require.ensure([], () => {
    // eslint-disable-next-line global-require,import/no-dynamic-require
    const Module = require(`./${name}`).default;
    new Module(el); // eslint-disable-line no-new
  });
}
