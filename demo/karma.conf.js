const webpackConfig = require('../gulpfile.js/lib/webpack-multi-config');
const path = require('path');
const log = require('fancy-log');

const testComponents = path.join(process.cwd(), '/components/**/*.test.js');
const testScripts = path.join(process.cwd(), '/javascripts/**/*.test.js');

log('Testing Started');
log(testComponents);
log(testScripts);
const karmaConfig = {
  frameworks: ['mocha', 'sinon-chai'],
  files: [testComponents, testScripts],
  preprocessors: {},
  webpack: webpackConfig('test'),
  singleRun: false,
  reporters: ['mocha', 'notify', 'bamboo'],
  browsers: ['PhantomJS'],
};

karmaConfig.preprocessors[testComponents] = ['webpack'];
karmaConfig.preprocessors[testScripts] = ['webpack'];

module.exports = (conf) => {
  conf.set(karmaConfig);
};
