# Viu Launchpad
Launchpad is our proven frontend build stack and toolkit to get projects of the ground quickly. It's based on Gulp and an assortment of handy tasks to test, build, optimize and deploy frontends. Launchpad is based on the popular [GulpStarter](https://github.com/vigetlabs/gulp-starter) by Viget.

## Release Notes / Breaking Changes
* 2.0.0 - 2.0.2: Release 2.0 brings fundamental (breaking) updates to VIU Launchpad
  * Based on Node 8 LTS / NPM 5 
  * All major dependencies updated. Webpack updated to v3.10. See [webpack 3 release notes](https://medium.com/webpack/webpack-3-official-release-15fd2dd8f07b) for how to configure webpack in order to improve javascript performance.
  * Improved support for TypeScript v2.7. Including TSLint.
  * Switched to babel-preset-env
  * Updated /demo project
* 1.0.0: VIU is proud to release version 1.0.0 of the VIU Launchpad. Highlights are:
  * Starting from our first 'stable' release, we will be following strict semVer naming conventions.
  * Therefore we will not have any braking changes without clear labelling.
  * This release is meant for Node LTS 6.*.

## Get started
Make sure you have node.js version 8.* (LTS) installed and selected (if you use nvm).
Create the following folder structure or use the example configuration from the ./demo/ folder.

* assets
  * fonts
  * icons (are automatically put into an icon font
  * images
  * sprites (are automatically added to a svg sprite)
  * static (are just copied to the root folder of the output)
  * webapp (are copied to the root folder of the output and updated with revisioned urls)
* components
  * {componentname}
    * {componentname}.js
    * {componentname}.scss
    * {componentname}.html
    * {componentname}.json
* data
  * global.json (file that holds all global data)
  * [...] here you add a {templatename}.json for every template that needs specific data
* javascripts
  * global.js (if you follow the example configuration below)
* stylesheets
  * globals.scss (if you follow the example configuration below)
* templates
  * layouts
  * macros
  * index.html
  * {templatename}.html

**Media Credits:**
camera.mp4 (http://www.beachfrontbroll.com/)

Then copy the following example package.json into the project root, remove comments and adjust values as you see fit. Then execute `npm install` (See [notes](#Notes) for additional information, particularily for Windows users.)
The generated package.json can be extended to override configuration values and to make project properties available in templates (see the [HTML](#HTML) section below). A possible package.json file could look like this (remove comments if you copy this into your package.json and execute `npm install` again to load dependencies):

```json
{
  "name": "My Project Name",
  "version": "0.1.0",
  "description": "My magic frontend project",
  "scripts": {
    "gulp": "gulp --gulpfile node_modules/@viu/launchpad/gulpfile.js/index.js",
    "develop": "npm run gulp",
    "lint": "npm run gulp lint",
    "demo": "npm run gulp production && npm run gulp server",
    "production": "npm run gulp production",
    "test": "npm run gulp production && karma start node_modules/@viu/launchpad/karma.conf.js --single-run"
  },
  "description": "Example Project using VIULaunchpad",
  "engines": {
    "node": "8.9.4",
    "npm": "5.6.0"
  },
  "homepage": "https://www.viu.ch",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/viu/LaunchpadExample.git"
  },
  "devDependencies": {
    "@viu/launchpad": "^2.0.3"
  },
  "config": {
    "root": {
      "src": "../../../src/" /* if you do not specify this, the folders mentioned above are expected in the project root */
    },
    "tasks": {
      "js": {
        "entries": {
          "global": [
            "./global.js"
          ]
        },
        "plugins": [
          {
            "reference": "jQuery",
            "name": "jquery"
          }
        ]
      },
      "browserSync": {
        "open": false,
        "cors": true,
        "rewrites":  { "from": ".", "to": "/index.html"} /* for projects that use push-state and need to send the same html file for all requests */
      },
      "iconFont": {
        "fontHeight": 1001,
        "normalize": true
      },
      "production": {
        "rev": true
      },
      "lint": {
        "js": {
          "config": ".eslintrc.yml" /* here we specify a custom eslint-config, that sits in the root-folder of your project */
        }
      },
      "myCustomTask": { /* the configuration for your customDevTasks is added the same way as all other tasks configuration */
        "src": "assets/myCustomAssetSource",
        "dest": "myCustomAssetDestination",
        "extensions": ["css", "js", "html", "svg"]
      }
    },
    "customDevTasks": ["myCustomTask"], /* the task files must be placed in a folder "tasks" below the project root (config.root.src). These tasks are not executed on production build */
    "customProdTasks": ["myCustomTask"] /* the task files must be placed in a folder "tasks" below the project root (config.root.src). These tasks are not executed on development build */
  },
  "license": "MIT",
  "dependencies": {
    "jquery": "^3.2.1"
  }
}
```

## Modes

### Development (Default)
The `npm run gulp` command runs the default task, defined in gulpfile.js/tasks/default.js. All files will compile in development mode (uncompressed with source maps). [BrowserSync](http://www.browsersync.io/) will serve up files to `localhost:3000` and will stream live changes to the code and assets to all connected browsers. Additional BrowserSync tools are available on `localhost:3001`.
The `npm run develop` command has the same effect.


### Run JavaScript Tests (currently untested)
```bash
npm run test
```
Test files located in `__tests__` folders are picked up and run using
Before using tests you need to install karma with `npm install karma --save-dev`
[Karma](http://karma-runner.github.io/0.12/index.html), [Mocha](http://mochajs.org/), [Chai](http://chaijs.com/), and [Sinon](http://sinonjs.org/). The test script right now first compiles a production build, and then, if successful runs Karma. This is nice when using something like [Travis CI](https://travis-ci.org/vigetlabs/gulp-starter) in that if an error occurs during the build step, Travis alerts me that it failed. To pass, the files have to compile properly AND pass the JS tests.

### Run Linting on JavaScript- and Sass-Source files (with [eslint](http://eslint.org) eslint and [sass-lint](https://github.com/sasstools/sass-lint))
```bash
npm run lint
```

This will check all JavaScript- and Sass-Source files for errors and warnings, according to the specified linter config. By default the subfolders `vendor` and `generated` will be excluded and files in there will not be checked. 

The eslint-config inherits from the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript) and adds the environments for `browser`, `node` and `mocha` for testing. Additionally the eslint-plugin `import` is installed to check failed imports. If you need more environments, plugins and or rules, have a look at gulfile.js/eslintrc.yml.

Sass is linted via sass-lint, the config-file is in gulpfile.js/sass-lint.yml. The config extends from [the default rules](https://github.com/sasstools/sass-lint/blob/develop/lib/config/sass-lint.yml) and adds more strict configuration for classic BEM conventions and is quite strict. Feel free to configure according to your projects specifications.

### Build production-ready files
```bash
npm run production
```

This will compile revisioned and compressed files to `./public`. To build production files and preview them localy, run 

### Demo
```bash
npm run demo
```
This will start a static server that serves your production files to http://localhost:5000. This is primarily meant as a way to preview your production build locally, not necessarily for use as a live production server.

### Debug complex problems
```bash
npm run debug
```

This will compile revisioned and files to `./public`, in the same way that `npm run production` does. However, this command will not minify / uglify your code, but rather keep the readable JavaScript.  

## Target specific variables

Add capability to define build-target-specific variables. These variables can be used in javascript files, and will be replaced to the configured value at compile-time.

`config.tasks.js.targetBuildVariables`
Usage:
1. List the build-target-specific variables you like to use in your scripts, in tasks.js.targetBuildVariables
2.A Define new npm scripts for all the target-environments you like to have, Environment-Variables to a specific value.
or: 
2.B Just set the Environment-Variables (E.g. 'API_USERS') directli via CLI (plattform-specific command)
3. Use the variables in your javascript files. Note that 'API_USERS' will be available as __API_USERS__ in your Script.

Basic scenario would be that you want to have different build, which test different API's, for instance:
Test-API on: "test.example.com"
Develop-API on: "dev.example.com"
Example package.JSON: (Only with specific 'develop' script. You probably have to define the other Scripts like 
"production_test", "production_develop" etc. as well.)
```
    "tasks": {
      "js": {
        "targetBuildVariables": [
          "API_USERS",
          "API_PRODUCTS"
        ]
      }
    },
    "scripts": {
       "develop": "gulp --gulpfile node_modules/@viu/launchpad/gulpfile.js/index.js",
       "develop_test": "cross-env API_USERS=test.example.com/getusers API_PRODUCTS=test.example.com/getproducts npm run develop",
       "develop_dev": "cross-env API_USERS=dev.example.com/getusers API_PRODUCTS=dev.example.com/getproducts npm run develop"
    }
```

**IMPORTANT:** Note the `cross-env` call at the beginning of the npm-scripts `develop_test` and `develop_dev`. The `cross-env` package will make sure that setting the environment-variable (e.g. `API_USERS`) will work cross-platform.

With this configuration, you can use your custom variables in your javascript files like following:

```
console.log(__API_USERS__);
console.log(__API_PRODUCTS__);
```

```
npm run develop_test
=> 'test.example.com/getusers'
=> 'test.example.com/getproducts'
``` 

```
npm run develop_dev
=> 'dev.example.com/getusers'
=> 'dev.example.com/getproducts'
```
Note: If you have set the environment variable directly via CLI, you just would run 'npn run develop (or whatever)'.


## Features
- **CSS:** [Sass](http://sass-lang.com/) (indented, scss, or both)
  - Libsass (node-sass) for super fast compiles
  - Autoprefixer
- **JS:** Modular ES6 with [Babel](http://babeljs.io/) and [Webpack](http://webpack.github.io/)
  - Async requires
  - Multiple bundles
  - Shared modules
  - Source Maps
- **HTML**: Static templating with [Nunjucks](https://mozilla.github.io/nunjucks/) and [gulp-data](https://github.com/colynb/gulp-data)
- **Images:**
  - **SVG Sprites**: Compiles a spritesheet from a folder of SVGs
  - Compression with image-min
- **Fonts:**
  - **Icon Fonts:** Generate from a folder of SVGs
  - Folder and `.sass` mixin for including WebFonts
- **Development Mode:**
  - File Watching and Live Reloading with [BrowserSync](http://www.browsersync.io/)
  - Source Maps
- **Production Builds:**
  - JS and CSS are uglified and minified
  - All filneames are revisioned with an md5 hash, a `rev-manifest.json` file is genrearted and all asset references are updated in html, css, and js
  - File size reporting
  - Local production sever for testing
- **Testing:**
  - JS test examples with Karma, Mocha, Chai, Sinon
- **Linting:**
  - JS linting with eslint and Sass-linting with sass-lint

## Task Details

### JS

Modular ES6 and/or Typescript with [Babel](http://babeljs.io/) and [Webpack](http://webpack.github.io/).

### CSS

Your Sass gets run through Autoprefixer, so don't prefix! The examples use the indented `.sass` syntax, but use whichever you prefer.

### HTML
```
gulpfile.js/tasks/html
```
Robust templating with [Nunjucks](https://mozilla.github.io/nunjucks/). 

Components automatically load the datafile (in the same folder and with the same name as the component) and combine this data object with data that can be provided directly in the template where the component is included. Data provided via the layout is overwritting the dafault data taken from the component folder.

Components are used via the component-tag which is not Nunjucks standard functionality but added in VIU Launchpad. usage:
```
{% component 'test', exampleComponentData %}
```

You may configure this task to use gulp-cached to only rebuild, if the file has actually changed during development.

In projects with many html templates, this  may improve performance. As a side-effect, the templates will not be rebuilt, if only the json is edited.

```
"html": {
        "cache": true,
      },
```

The data/global.json and the package.json are merged recursively with the page template of the current page and the merged object is available in the template.

### IconFont

SVGs added to `src/icons` will be automatically compiled into an iconFont, and output to `./public/fonts`. At the same time, a `.sass` file will be output to `src/stylesheets/generated/_icons.sass`. This file contains mixins and classes based on the svg filename. If you want to edit the template that generates this file, it's at `gulpfile.js/tasks/iconFont/template.sass`

#### Usage:
With generated classes:
```
<span class="icon -twitter"></span>
```

With mixins:
```sass
.lil-birdy-guy
  +icon--twitter
```

```scss
.lil-birdy-guy {
  @include icon--twitter;
}
```

```html
<span class="lil-birdy-guy"></span>
```

*Don't forget about accessibility!*

```html
<span aria-label="Twitter" class="icon -twitter"></span>
<!-- or -->
<div class="icon -twitter"><span class="screen-reader">Twitter</span></div>
```

### SVG Sprites
```
gulpfile.js/tasks/svgSprite
```
SVGs sprites are super powerful. This particular setup allows styling 2 different colors from your css. You can have unlimited colors hard coded into your svg.  

In the following example, the first path will be `red`, the second will be `white`, and the third will be `blue`. Paths **without a fill attribute** will inherit the `fill` property from css. Paths with **fill="currentColor"** will inherit the current css `color` value, and hard-coded fills will not be overwritten, since inline styles trump css values.

```sass
.sprite
  fill: red
  color: white
```

```svg
  <svg xmlns="http://www.w3.org/2000/svg">
    <path d="..."/>
    <path fill="currentColor" d="..."/>
    <path fill="blue" d="..."/>
  </svg>
```

We've included a helper to generate the required svg markup in `src/html/macros/helpers.html`, so you can just do:
```html
  {{ sprite('my-icon') }}
```
Which generates:

```html
  <span class='sprite -my-icon'>
    <svg viewBox="0 0 1 1"><use xlink:href='images/spritesheets/sprites.svg#my-icon' /></use></svg>
  </span>
```

We recommend setting up your SVGs on a square canvas, centering your artwork, and expanding/combining any shapes of the same color. This last step is important.

### Static Files (favicons, app icons, etc.)
There are some files that belong in your root destination directory that you won't want to process or revision in production. Things like [favicons, app icons, etc.](http://realfavicongenerator.net/), should go in `src/static`, and will get copied over to `public` as a last step (after revisioning in production). *Nothing* should ever go directly in `public`, since it gets completely trashed and re-built when running the `default` or `production` tasks.

### Webapp Files (serviceworkers, .htaccess)
There are some files that belong in your root destination directory that you want to go into webroot and be updated with revisioned urls. Files like service workers, web workers, your manifest or .htaccess configuration should go in `src/webapp`, and will get copied over to `public`. *Nothing* should ever go directly in `public`, since it gets completely trashed and re-built when running the `default` or `production` tasks.

## Notes
Launchpad requires Node 8.9.x or higher. We recommend using [NVM](https://github.com/creationix/nvm) to manage versions.

### For Windows Users
- Install NVM (Node version manager) for PowerShell: https://github.com/aaronpowell/ps-nvmw
- Install node via NVM, and use it
- Before attempting to do 'npm install', install one of the 2.7.* versions of Python (https://www.python.org/downloads/)
- Before attempting to do 'npm install', check that you have cl.exe (C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\cl.exe); if not, open Visual Studio and create a C++ project - it will ask you to install C++ tools, install these and reopen powershell
- Open PowerShell as administrator to do npm install (and don't forget 'Set-NodeVersion v6.10.1')

#### Configuration
All configuration that is possible can be found in node_modules/@viu/launchpad/gulpfile.js/config.json and can be overwritten in the "config" object within the package.json.
