'use strict';

const path = require('path');
const fs = require('fs');

module.exports = function webpackManifest(publicPath, dest, filename) {
  // eslint-disable-next-line no-param-reassign
  filename = filename || 'rev-manifest.json';

  return function returnFunction() {
    this.plugin('done', (webpackStats) => {
      const stats = webpackStats.toJson();
      const chunks = stats.assetsByChunkName;
      const manifest = {};

      Object.keys(chunks).forEach((key) => {
        const originalFilename = `${key}.js`;
        manifest[path.join(publicPath, originalFilename)] = path.join(publicPath, chunks[key]);
      });

      fs.writeFileSync(
        path.join(process.cwd(), dest, filename),
        JSON.stringify(manifest)
      );
    });
  };
};
