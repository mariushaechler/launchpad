'use strict';

const config = require('./configReader.js');

if (!config.tasks.html) return;

const render = require('gulp-nunjucks-render');
const path = require('path');
const fs = require('fs');
const merge = require('merge');
const fileExists = require('./fileExists');
const removeBOM = require('./removeBOM');
const handleErrors = require('./handleErrors');

/**
 * {% component 'name', {title: 'Example', subtitle: 'An example component'} %}
 */
const ComponentTag = function componentTag() {
  this.tags = ['component'];

  // eslint-disable-next-line no-unused-vars
  this.parse = (parser, nodes, lexer) => {
    const token = parser.nextToken();

    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(token.value);

    return new nodes.CallExtension(this, 'run', args);
  };

  this.run = (context, name, data) => {
    // components are organized in this manner: components/componentName/componentName.html
    // Note that the component is only given the data object, and the context is not exposed.
    // data ist merged from data files (components/componentName/componentName.json)
    // and the data provided in the component tag
    // data provided in the component tag overrides default content from component
    // check if file exists - otherwise provide empty data
    const dataPath = path.resolve(config.root.src, 'components', name, `${name}.json`);
    const filePath = path.resolve(config.root.src, 'components', name, `${name}.html`);
    if (fileExists(dataPath)) {
      let defaultData = {};
      try {
        defaultData = JSON.parse(removeBOM(fs.readFileSync(dataPath, 'utf8')));
      } catch (e) {
        handleErrors(e);
      }

      // eslint-disable-next-line no-param-reassign
      data = merge(true, defaultData, data);
      // could be replaced with the following line to merge recursively
      // not sure what makes more sense
      // data = merge.recursive(true, defaultData, data);
    }

    const env = render.nunjucks.configure(
      [path.join(config.root.src, config.tasks.html.root)],
      { autoescaping: false, watch: false }
    );
    env.addExtension('component', new ComponentTag());
    if (config.tasks.html.filters) {
      for (let i = 0; i < config.tasks.html.filters.length; i += 1) {
        let filterPath;
        let filter;

        try {
          filterPath = path.resolve(
            process.cwd(),
            config.tasks.html.filters[i]
          );
          // eslint-disable-next-line global-require
          filter = require(filterPath);
        } catch (errNotCalledFromGulpAndInitCWDUndefined) {
          filterPath = path.resolve(
            process.cwd(),
            config.root.base,
            config.tasks.html.filters[i]
          );
          // eslint-disable-next-line global-require
          filter = require(filterPath);
        }
        env.addFilter(filter.name, filter.filter);
      }
    }

    return context.env.filters.safe(render.nunjucks.render(filePath, data));
  };
};

module.exports = new ComponentTag();
