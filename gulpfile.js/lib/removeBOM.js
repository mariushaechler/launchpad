'use strict';

module.exports = fileContents => fileContents.replace(/^\ufeff/g, '');
