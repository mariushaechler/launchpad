'use strict';

const gulp = require('gulp');

gulp.task('lint', ['eslint', 'sasslint', 'tslint']);
