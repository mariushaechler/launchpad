// Copies files from one folder to another
// Unlike the static task, this task is run
// before revving takes place and thus the
// files in this task are included in gulp-rev

'use strict';

const config = require('../lib/configReader.js');
const gulp = require('gulp');
const path = require('path');

const paths = {
  src: [
    path.join(config.root.src, config.tasks.webapp.src, '/**'),
    // since gulp.src does not conside /** to be 'all folders and their files' but
    // only 'all folders and their non-.hidden files. We add a second task to copy
    // .hidden files.
    path.join(config.root.src, config.tasks.webapp.src, '/**/.*')
  ],
  dest: path.join(config.root.dest, config.tasks.webapp.dest)
};

const webappTask = () => gulp.src(paths.src)
  .pipe(gulp.dest(paths.dest));

gulp.task('webapp', webappTask);
module.exports = webappTask;
