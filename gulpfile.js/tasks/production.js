'use strict';

const config = require('../lib/configReader.js');
const gulp = require('gulp');
const gulpSequence = require('gulp-sequence');
const getEnabledTasks = require('../lib/getEnabledTasks');
const path = require('path');
const del = require('del');

gulp.task('copyTemplates', () => {
  const srcPath = path.join(config.root.dest, config.tasks.static.dest, 'templates', '**');
  const destPath = path.join(config.root.dest, config.tasks.static.dest);
  return gulp.src(srcPath).pipe(gulp.dest(destPath));
});

gulp.task('removeTemplatesFolder', ['copyTemplates'], () => {
  const srcPath = path.join(config.root.dest, config.tasks.static.dest, 'templates');
  del([srcPath], () => {});
});

const productionTask = (cb) => {
  global.production = true;
  const tasks = getEnabledTasks('production');
  gulpSequence(
    'clean',
    tasks.assetTasks,
    tasks.codeTasks,
    tasks.customProdTasks,
    'webapp',
    config.tasks.production.rev ? 'rev' : false,
    'size-report',
    'static',
    cb
  );
};

gulp.task('production', productionTask);
module.exports = productionTask;
