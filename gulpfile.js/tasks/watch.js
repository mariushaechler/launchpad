'use strict';

const config = require('../lib/configReader.js');
const gulp = require('gulp');
const path = require('path');
const watch = require('gulp-watch');

const watchTask = () => {
  const watchableTasks = ['fonts', 'iconFont', 'images', 'svgSprite', 'html', 'css', 'static'];

  watchableTasks.forEach((taskName) => {
    const task = config.tasks[taskName];
    if (task) {
      let glob = [];
      glob.push(path.join(config.root.src, task.src, `**/*.{${task.extensions.join(',')}}`));

      // Ignoring temporary files pattern, as recommended here:
      // https://github.com/floatdrop/gulp-watch/issues/238
      // https://github.com/floatdrop/gulp-watch/issues/242
      if (task.excludeExtensions) {
        glob.push(`!${path.join(config.root.src, task.src, `**/*{${task.excludeExtensions.join(',')}}`)}`);
      }

      if (task.watchSrc) {
        glob = [];
        task.watchSrc.forEach((watchPath) => {
          glob.push(path.join(config.root.src, watchPath, `**/*.{${task.extensions.join(',')}}`));
          if (task.watchExcludeExtensions) {
            glob.push(path.join(`!${config.root.src}`, watchPath, `**/*{${task.watchExcludeExtensions.join(',')}}`));
          }
        });
      }
      watch(glob, () => {
        // eslint-disable-next-line global-require
        require(`./${taskName}`)();
      });
    }
  });
};

gulp.task('watch', ['browserSync'], watchTask);
module.exports = watchTask;
