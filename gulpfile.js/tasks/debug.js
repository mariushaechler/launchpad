'use strict';

const gulp = require('gulp');
const gulpSequence = require('gulp-sequence');
const getEnabledTasks = require('../lib/getEnabledTasks');

const debugTask = (cb) => {
  const tasks = getEnabledTasks('debug');

  gulpSequence(
    'clean',
    tasks.assetTasks,
    tasks.codeTasks,
    tasks.customDevTasks,
    'webapp',
    'static',
    cb
  );
};

gulp.task('debug', debugTask);
module.exports = debugTask;
