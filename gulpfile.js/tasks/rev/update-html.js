'use strict';

const gulp = require('gulp');
const config = require('../../lib/configReader.js');
const revReplace = require('gulp-rev-replace');
const path = require('path');
const gulpIf = require('gulp-if');

// 5) Update asset references in HTML
gulp.task('update-html', () => {
  const manifest = gulp.src(path.join(config.root.dest, '/rev-manifest.json'));
  return gulp.src(path.join(config.root.dest, config.tasks.html.dest, '/**/*.html'))
    .pipe(gulpIf(config.tasks.production.rev, revReplace({ manifest })))
    .pipe(gulp.dest(path.join(config.root.dest, config.tasks.html.dest)));
});
