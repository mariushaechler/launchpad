'use strict';

const config = require('../lib/configReader.js');

if (!config.tasks.js) return;

const webpackConfig = require('../lib/webpack-multi-config')('debug');
const gulp = require('gulp');
const logger = require('../lib/compileLogger');
const webpack = require('webpack');

const webpackDebugTask = (callback) => {
  webpack(webpackConfig, (err, stats) => {
    logger(err, stats);
    callback();
  });
};

gulp.task('webpack:debug', webpackDebugTask);
module.exports = webpackDebugTask;
