'use strict';

const config = require('../lib/configReader.js');

if (!config.tasks.html) return;

const browserSync = require('browser-sync');
const data = require('gulp-data');
const gulp = require('gulp');
const gulpif = require('gulp-if');
const gulpcached = require('gulp-cached');
const handleErrors = require('../lib/handleErrors');
const htmlmin = require('gulp-htmlmin');
const path = require('path');
const render = require('gulp-nunjucks-render');
const fs = require('fs');
const componentTag = require('../lib/nunjucksExtensionComponent');
const fileExists = require('../lib/fileExists');
const removeBOM = require('../lib/removeBOM');
const merge = require('merge');

// get data for templates
const getData = function getTemplateData(file) {
  let globalData = {};
  let packageData = {};

  // check for global.json
  const globalDataPath = path.resolve(config.root.src, config.tasks.data.src, 'global.json');
  if (fileExists(globalDataPath)) {
    try {
      globalData = JSON.parse(removeBOM(fs.readFileSync(globalDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }
  // add package json into global data
  const packageDataPath = path.resolve(config.root.base, 'package.json');
  if (fileExists(packageDataPath)) {
    try {
      packageData = JSON.parse(removeBOM(fs.readFileSync(packageDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }
  packageData = { package: packageData };
  globalData = merge.recursive(true, packageData, globalData);

  // find relative data file
  let pageDataPath = path.resolve(config.root.src, config.tasks.data.src, file.relative);

  // replace duplicated folder separator
  pageDataPath = pageDataPath.replace('.html', '.json').replace(path.sep + path.sep, path.sep);

  // remove folders specified in config (so we don't need to put data files into /data/templates
  config.tasks.data.removeFromPath.forEach((pathToRemove) => {
    // eslint-disable-next-line no-param-reassign
    pathToRemove = pathToRemove.replace('/', path.sep);
    pageDataPath = pageDataPath.replace(pathToRemove, '');
  });

  // check if file exists - otherwise provide empty data
  let pageData = {};
  if (fileExists(pageDataPath) && !fs.lstatSync(pageDataPath).isDirectory()) {
    try {
      pageData = JSON.parse(removeBOM(fs.readFileSync(pageDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }

  return merge.recursive(true, globalData, pageData);
};

// this solution appears to only be working if more than one entry is placed in excludeFolders
const exclude = path.normalize(`!${config.root.src}**/{${config.tasks.html.excludeFolders.join(',')}}/**`);
const srcPaths = [path.join(config.root.src, config.tasks.html.src), exclude];

// main task
const htmlTask = function processHTML() {
  return gulp.src(srcPaths)
    .pipe(data(getData))
    .on('error', handleErrors)
    .pipe(gulpif(!global.production && config.tasks.html.cache, gulpcached('nunjucks-html')))
    .pipe(render({
      // Add node modules to path of gulp-nunjucks in order to be able to use templates
      // from node modules. Example: VIU Styleguide
      path: [path.join(config.root.src, config.tasks.html.root), '../../../node_modules'],
      envOptions: {
        watch: false
      },
      manageEnv(env) {
        env.addExtension('component', componentTag);
        if (config.tasks.html.filters) {
          for (let i = 0; i < config.tasks.html.filters.length; i += 1) {
            let filterPath;
            let filter;

            try {
              filterPath = path.resolve(process.cwd(), config.tasks.html.filters[i]);
              // eslint-disable-next-line global-require
              filter = require(filterPath);
            } catch (errNotCalledFromGulpAndInitCWDUndefined) {
              filterPath = path.resolve(
                process.cwd(),
                config.root.base, config.tasks.html.filters[i]
              );
              // eslint-disable-next-line global-require
              filter = require(filterPath);
            }
            env.addFilter(filter.name, filter.filter);
          }
        }
      }
    }))
    .on('error', handleErrors)
    .pipe(gulpif(global.production, htmlmin(config.tasks.html.htmlmin)))
    .pipe(gulp.dest(path.join(config.root.dest, config.tasks.html.dest)))
    .on('end', browserSync.reload);
};

gulp.task('html', htmlTask);
module.exports = htmlTask;
