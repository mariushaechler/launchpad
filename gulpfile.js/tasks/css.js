'use strict';

const config = require('../lib/configReader.js');

if (!config.tasks.css) return;

const gulp = require('gulp');
const gulpif = require('gulp-if');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const handleErrors = require('../lib/handleErrors');
const autoprefixer = require('gulp-autoprefixer');
const path = require('path');
const cssnano = require('gulp-cssnano');

const paths = {
  src: path.join(config.root.src, config.tasks.css.src, `/**/*.{${config.tasks.css.extensions}}`),
  dest: path.join(config.root.dest, config.tasks.css.dest)
};

const cssTask = () => gulp.src(paths.src)
  .pipe(gulpif(!global.production, sourcemaps.init()))
  .pipe(sass(config.tasks.css.sass))
  .on('error', handleErrors)
  .pipe(autoprefixer(config.tasks.css.autoprefixer))
  .pipe(gulpif(global.production, cssnano(config.tasks.css.min)))
  .pipe(gulpif(!global.production, sourcemaps.write()))
  .pipe(gulp.dest(paths.dest))
  .pipe(browserSync.stream());

gulp.task('css', cssTask);
module.exports = cssTask;
