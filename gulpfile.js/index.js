/*
gulpfile.js
===========
Rather than manage one giant configuration file responsible
for creating multiple tasks, each task has been broken out into
its own file in gulpfile.js/tasks. Any files in that directory get
automatically required below.

To add a new task, simply add a new task file into that directory.
gulpfile.js/tasks/default.js specifies the default set of tasks to run
when you run `gulp`.
*/

'use strict';

const path = require('path');
const requireDir = require('require-dir');
const config = require('./lib/configReader.js');
const log = require('fancy-log');
const colors = require('ansi-colors');

let lp;

try {
  // eslint-disable-next-line
  lp = require(path.resolve(process.env.INIT_CWD,'node_modules/@viu/launchpad/package.json'));
} catch (Ex) {
  lp = {
    name: 'VIU Launchpad',
    version: '?',
  };
}

// print VIU logo and launchpad version and project version to console
log(colors.red(`Starting ${lp.name}@${lp.version}

                  viiv                 
                 UUUUUU                 
                  vuuv                 
                                        
viuuuu     iiiiiiiUUUUi  uuuuu     uuuuu
 UUUUUi   iUUUUI iUUUUi  UUUUU     UUUUU
 iUUUUI   IUUUU  iUUUUi  UUUUU     UUUUU
  vUUUUv iUUUUi  iUUUUi  UUUUU     UUUUU
   UUUUU uUUUv   iUUUUi  UUUUU     UUUUU
   iUUUUUUUUU    iUUUUi  UUUUU    iUUUUU
    iUUUUUUUi    iUUUUi  iUUUUUUUUUUUUUU
     iVVVVVi     vUUUUv   vVUUUUVv  VIUU
          
`));

// Require all tasks in gulpfile.js/tasks, including subfolders
requireDir('./tasks', { recurse: true });
if ((config.customDevTasks && config.customDevTasks.length > 0) ||
    (config.customProdTasks && config.customProdTasks.length > 0)) {
  requireDir(path.join(config.root.src, 'tasks'), { recurse: true });
}
